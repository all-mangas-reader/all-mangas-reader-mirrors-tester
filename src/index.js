const express = require("express");
const favicon = require('serve-favicon');
const bodyParser = require('body-parser')
const path = require('path');
const app = express();
const ImplLoader = require("./impl/impl-loader")
const Pool = require("./pooler")

const port = process.env.PORT || 3003;


(async () => {
    let websites = await ImplLoader.getWebsites(), wss = []
    for (let name in websites) {
        if (!websites.hasOwnProperty(name)) continue;
        let ws = websites[name]
        if (ws.type === "abstract" || name === "__LOADED__") continue;
        wss.push(ws)
    }

    console.log("Starting server")
    app.use(favicon(path.join(__dirname, '/favicon.ico')));
    app.use("/static", express.static(path.join(__dirname, "public")));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.set('view engine', 'pug');
    app.set('views', path.join(__dirname, '/views'));
    app.get("/", (req, res) => {
        res.render('index', {
            wss: wss
        })
    });
    app.get("/reset", async (req, res) => {
        ImplLoader.resetWebsites()
        let nb = 0;
        for (let name in websites) {
            if (!websites.hasOwnProperty(name)) continue;
            let ws = websites[name]
            if (ws.type === "abstract" || name === "__LOADED__") continue;
            res.write(name + "\n");
            nb++;
        }
        res.write(nb + " loaded")
        res.end()
    });
    app.get("/:wsname", async (req, res) => {
        var wsname = req.params.wsname;
        res.render('website', {
            ws: await ImplLoader.getDescription(wsname),
            search_phrase: req.query.search_phrase,
            manga_url: req.query.manga_url,
            chapter_url: req.query.chapter_url
        })
    });
    app.get("/runtest/:wsname", async (req, res) => {
        var wsname = req.params.wsname;
        await Pool.requestRun(wsname, res)
        res.end();
    });
    app.post("/runtest/:wsname", async (req, res) => {
        var wsname = req.params.wsname;

        let options = {}
        if (req.body.search_phrase) options.search_phrase = req.body.search_phrase
        if (req.body.manga_url) options.manga_url = req.body.manga_url
        if (req.body.chapter_url) options.chapter_url = req.body.chapter_url

        await Pool.requestRun(wsname, res, options)
        res.end()
    });
    app.listen(port, () => {
        console.log(`Running on http://localhost:${port}`)
    });
})()