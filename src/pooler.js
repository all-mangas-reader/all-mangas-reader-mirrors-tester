const Loader = require("./loader")

let Pool = function() {
    this.pool = 10 // no more than 10 requests at a time

    /**
     * We have a pool of request and can execute just one at a time... 
     * (due mainly to global objects like $ needed to execute code from within the implementations), 
     * if we execute two request simultaneously, both fail...
     * @param {*} res 
     * @param {*} options 
     */
    this.requestRun = async function (mirrorName, res, options) {
        let self = this;
        return new Promise((resolve, reject) => {
            (async function wait() {
                if (self.pool > 0) {
                    self.pool--
                    let loader = new Loader(mirrorName)
                    let returned = await loader.runMirror(res, options)
                    self.pool++
                    resolve(returned)
                } else {
                    setTimeout(wait, 250)
                }
            })()
        })
    }
}
module.exports = new Pool()