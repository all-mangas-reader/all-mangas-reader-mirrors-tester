const ImplLoader = require("../impl/impl-loader")

/**
 * Initialize global objects and functions which may be called from implementations 
 * and work in the browser
 */
require("./amr")
const { XMLHttpRequest } = require("xmlhttprequest");
global.XMLHttpRequest = XMLHttpRequest
global.btoa = function(str) {
    return Buffer.from(str).toString('base64')
}
global.atob = function(str) {
    return Buffer.from(str, 'base64').toString('ascii')
}
global.window = global

/**
 * Hook for the messages needed from tests
 * tests are the same as those from the laboratory in amr extension, just hook the browser sendMessage
 * function which is the only one needed
 */
var Browser = function() {
    this.runtime = {}
    this.runtime.sendMessage = (message) => {
        let result
        if ("lab" === message.action) {
            if (message.url && message.url.indexOf("//") === 0) 
                message.url = "http:" + message.url;
            result = this.runFunction(message);
        }
        if (result === undefined) console.error("No handler for message action " + message.action)
        return result
    }
    this.runFunction = async function(message) {
        // load implementation for mirror
        return new Promise(async (resolve, reject) => {
            try {
                let impl = await ImplLoader.getImplementation(message.mirror);
                if (message.torun === "search") {
                    let res = await impl.getMangaList(message.search)
                    resolve(res);
                }
                else if (message.torun === "chapters") {
                    let lst = await impl.getListChaps(message.url)
                    resolve(lst);
                }
                else if (message.torun === "loadChapterAndDo") {
                    let res = this.loadChapterAndDo(message, impl);
                    resolve(res);
                } 
                else if (message.torun === "getScanUrl") {
                    let img = $("<img src='' />")[0] //new Image(); not defined --> use $ because page was loaded before
                    await impl.getImageFromPageAndWrite(message.url, img);
                    (function wait() {
                        if ($(img).attr("src") && $(img).attr("src") !== "") {
                            return resolve($(img).attr("src"));
                        }
                        setTimeout(wait, 100);
                    })()
                }
            } catch (e) {
                reject(e);
            }
        });
    }
    this.loadChapterAndDo = async function(message, impl) {
        return new Promise(async (resolve, reject) => {
            try {
                let options = {}
                if (impl.test_options && impl.test_options.user_agent) {
                    // Some website need to have a user agent while loading pages from their site, some headers settings can be done through the implementation but this one need to be done through a test option
                    options.headers = {}
                    options.headers["user-agent"] = impl.test_options.user_agent
                }
                let doc = await amr.loadPage(message.url, options) // set agent to avoid some websites errors
                if (message.task === "containScans") {
                    resolve(
                        impl.isCurrentPageAChapterPage(doc, message.url)
                    );
                } else if (message.task === "informations") {
                    let infos = await impl.getInformationsFromCurrentPage(
                        doc, 
                        message.url
                    );
                    resolve(infos);
                } else if (message.task === "listScans") {
                    let imagesUrl = await impl.getListImages(doc, message.url);
                    resolve(imagesUrl);
                } else if (message.task === "wherenav") {
                    impl.doSomethingBeforeWritingScans(doc, message.url);
                    let where = await impl.whereDoIWriteScans(doc, message.url);
                    resolve(where.length);
                }
            } catch (e) {
                console.error(e)
                reject(e);
            }
        });
    }
}

module.exports = new Browser()