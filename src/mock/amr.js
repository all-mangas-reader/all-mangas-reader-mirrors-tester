const request = require('request')
const { JSDOM } = require('jsdom')

/**
 * Hook for the amr helper functions used in the implementations
 */
global.amr = {}

/**
 * Loads a page and return an element in which page is loaded
 * options : 
 *  - nocache: true to prevent page to be loaded from cache
 *  - preventimages: true to prevent images in page to be loaded
 *  - post: true to send message using POST
 *  - dataType: valid jQuery ajax dataType
 *  - data: object to send in accordance with its dataType
 * @param {*} url 
 * @param {*} options 
 */
amr.loadPage = function (url, options) {
    return new Promise((resolve, reject) => {
        let requestObj = { url: url }
        let headers = {}
        if (options && options.nocache) {
            headers["Cache-Control"] = "no-cache"
            headers["Pragma"] = "no-cache"
        }
        if (options && options.post) {
            requestObj.method = 'POST'
        }
        if (options && options.data !== undefined) {
            // TODO depend on dataType --> not set use formData
            if (options.dataType === "text") {
                //send data as plain text
                if (typeof options.data === "string") {
                    requestObj.body = options.data
                } else {
                    requestObj.body = Object.keys(options.data).map(key => key + "=" + options.data[key]).join("\n")
                }
            } else {
                //use formData
                requestObj.form = options.data
            }
        }
        if (options && options.headers !== undefined) {
            Object.assign(headers, options.headers)
        }
        /*if (options && options.crossdomain) { // NOT relevant in node
            ajaxObj.crossDomain = true
        }*/
        requestObj.headers = headers
        amr.addCookiesToRequest(requestObj, url)

        request(requestObj, (error, response, body) => {
            if (!error && response.statusCode == 200) {
                if (options && options.preventimages) {
                    body = body.replace(/<img/gi, '<noload')
                }
                try {
                    let dom = new JSDOM(body)
                    // just init jquery once --> we do not care if dom which initiate jquery is dead as the dom is passed a second arg each time in amr
                    global.$ = global.$ || (require('jquery'))(dom.window) // it's ugly but it is the only way to let the implementations script have $ working as in a browser
                    resolve($(dom.window.document));
                } catch (e) {
                    console.log("Error, unable to load DOM : ")
                    console.log(e)
                    console.log(body)
                    reject("Error, unable to load DOM : " + (e === null ? "null" : e.name + " : " + e.message + "\n" + e.stack) + "\nReturned body : \n" + body)
                }
            } else {
                console.log(error)
                console.log("Requested url " + requestObj.url + " failed : " + response.statusCode)
                console.log(body)
                /*if (response.statusCode === 503 && body != "") {
                    let dom = new JSDOM(body)
                    global.$ = global.$ || (require('jquery'))(dom.window)
                    eval($("script", dom).text().replace("4000", "0"))
                    amr.loadPage(url, options)
                }*/
                reject("Requested url " + requestObj.url + " failed : " + response.statusCode)
            }
        });
    });
}
/**
 * Loads a url and get JSON
 * options : 
 *  - nocache: true to prevent cache
 *  - post: true to send message using POST
 *  - dataType: valid jQuery ajax dataType
 *  - data: object to send in accordance with its dataType
 * @param {*} url 
 * @param {*} options 
 */
amr.loadJson = function (url, options) {
    return new Promise((resolve, reject) => {
        let requestObj = { url: url }
        let headers = {}
        if (options && options.nocache) {
            headers["Cache-Control"] = "no-cache"
            headers["Pragma"] = "no-cache"
        }
        if (options && options.post) {
            requestObj.method = 'POST'
        }
        if (options && options.data !== undefined) {
            // TODO depend on dataType --> not set use formData
            if (options.dataType === "text") {
                //send data as plain text
                if (typeof options.data === "string") {
                    requestObj.body = options.data
                } else {
                    requestObj.body = Object.keys(options.data).map(key => key + "=" + options.data[key]).join("\n")
                }
            } else {
                //use formData
                requestObj.form = options.data
            }
        }
        
        let contenttype = true
        if (options && options.nocontenttype) {
            contenttype = false
        }
        if (contenttype) headers['Content-Type'] = "application/json";

        if (options && options.headers !== undefined) {
            Object.assign(headers, options.headers)
        }
        if (options && options.referer) {
            headers["Referer"] = options.referer // can only work in nodejs env
        }
        requestObj.headers = headers
        amr.addCookiesToRequest(requestObj, url)

        request(requestObj, (error, response, body) => {
            if (!error && response.statusCode == 200) {
                if (typeof body === "string") {
                    try {
                        resolve(JSON.parse(body));
                    } catch (e) {
                        resolve(body);
                    }
                } else {
                    resolve(body);
                }
            } else {
                reject(error)
            }
        });
    });
}
/**
 * Get a variable value from a script tag, parse it manually
 * @param {*} varname 
 */
amr.getVariable = function (varname, doc) {
    let res = undefined
    // A document has been loaded, all document are loaded through loadPage and set $ globally even if it's ugly so that works
    $("script", doc).each(function (i) {
        res = amr.getVariableFromScript(varname, $(this).text())
        if (res !== undefined) return false
    })
    return res
}

/**
 * Get a variable value from a snippet
 */
amr.getVariableFromScript = function (varname, sc) {
    let res = undefined
    let rx = new RegExp("(var|let|const)\\s+" + varname + "\\s*=\\s*([0-9]+|\\\"|\\\'|\\\{|\\\[|JSON\\s*\\\.\\s*parse\\\()", "gmi")
    let match = rx.exec(sc)
    if (match) {
        let ind = match.index
        let varchar = match[2]
        let start = sc.indexOf(varchar, ind) + 1
        if (varchar.match(/[0-9]+/)) {
            res = Number(varchar)
        } else {
            if (varchar === '"' || varchar === "'") { // var is a string
                let found = false,
                    curpos = start,
                    prevbs = false;
                while (!found) {
                    let c = sc.charAt(curpos++)
                    if (c === varchar && !prevbs) {
                        found = true
                        break
                    }
                    prevbs = c === "\\"
                }
                res = sc.substring(start, curpos - 1)
            } else { // if (varchar === '[' || varchar === "{" || varchar === 'JSON.parse(') { // var is object or array or parsable
                let curpos = start + varchar.length - 1,
                    openings = 1,
                    opening = varchar === 'JSON.parse(' ? '(' : varchar,
                    opposite = varchar === '[' ? ']' : (varchar === '{' ? '}' : ')')
                while (openings > 0 && curpos < sc.length) {
                    let c = sc.charAt(curpos++)
                    if (c === opening) openings++
                    if (c === opposite) openings--
                }
                let toparse = sc.substring(start - 1 + varchar.length - 1, curpos)
                if (toparse.match(/atob\s*\(/g)) { // if data to parse is encoded using btoa
                    let m = /(?:'|").*(?:'|")/g.exec(toparse)
                    toparse = atob(m[0].substring(1, m[0].length - 1))
                }
                res = JSON.parse(toparse)
            }
        }
    }
    return res
}
/**
 * Stored cookies 
 */
amr.cookies = {}
/**
 * Set a cookie on a domain
 */
amr.setCookie = async function (setCookieObj) {
    if (amr.cookies[setCookieObj.domain] === undefined) amr.cookies[setCookieObj.domain] = []
    if (amr.cookies[setCookieObj.domain]
        .find(cook => cook.name === setCookieObj.name) === undefined) {
        amr.cookies[setCookieObj.domain].push(setCookieObj)
    }
}
/**
 * Set registered cookies on a request
 * @param {*} requestObj 
 * @param {*} url 
 */
amr.addCookiesToRequest = function(requestObj, url) {
    let host = amr.extractHostname(url)
    if (amr.cookies[host] !== undefined) {
        /*console.log("Attaching cookies to request " + url)
        requestObj.headers["Cookie"] = amr.cookies[host].map(cook => cook.name + '=' + cook.value).join("; ")*/
        var j = request.jar()
        for (let cook of amr.cookies[host]) {
            var cookie = request.cookie(cook.name + '=' + cook.value)
            cookie.httpOnly = false
            if (cook.expirationDate) {
                let dt = new Date()
                dt.setTime(cook.expirationDate)
                cookie.setExpires(dt)
            }
            cookie.path = cook.domain
            j.setCookie(cookie, "https://" + host)
            console.log(cookie)
        }
        requestObj.jar = j
    }
}
/**
 * Extract the full host name
 * @param {*} url 
 */
amr.extractHostname = function(url) {
    var hostname;
    //find & remove protocol (http, ftp, etc.) and get hostname

    if (url.indexOf("://") > -1 || url.indexOf("//") === 0) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];
    //find & remove "?"
    hostname = hostname.split('?')[0];

    return hostname;
}
/**
 * Extract the root domain of a url without subdomain
 * @param {*} url 
 */
amr.extractRootDomain = function(url) {
    var domain = extractHostname(url),
        splitArr = domain.split('.'),
        arrLen = splitArr.length;

    //extracting the root domain here
    //if there is a subdomain 
    if (arrLen > 2) {
        domain = splitArr[arrLen - 2] + '.' + splitArr[arrLen - 1];
        //check to see if it's using a Country Code Top Level Domain (ccTLD) (i.e. ".me.uk")
        if (splitArr[arrLen - 2].length == 2 && splitArr[arrLen - 1].length == 2) {
            //this is using a ccTLD
            domain = splitArr[arrLen - 3] + '.' + domain;
        }
    }
    return domain;
}