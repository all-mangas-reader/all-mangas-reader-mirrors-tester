/**
 * This is a node app aiming to run a test suite on a website implementation
 *  - the implementation is first loaded
 *  - the test suite from laboratory is ran
 *  - this is using additional parameters on the implementation using test attribute
 */

const default_test_options = {
    search_phrase: "one", /* Default search phrase, if the website does not return any entry for this search phrase, override it in the implementation */
    do_not_fail_if_chapters_empty: true, /* True to test another manga entry if chapters return nothing */
    chapters_attempt: 5, /* number of attemps to pick a random manga if chapters list is empty */
    images_restricted_domain: false /* set to true if the image can not be loaded outside of its domain */
}

const tests = require("./tests/tests")
const ImplLoader = require("./impl/impl-loader")
const linkifyHtml = require('linkifyjs/html');

/**
 * Class ensuring test suite run
 */
module.exports = class WebsiteTestRunner {
    constructor(mirrorName, options) {
        this.mirrorName = mirrorName
        this.testsResults = []
        this.options = options
    }

    /**
     * Load all defined options in their order of overriding
     */
    async loadTestOptions() {
        let impl = await ImplLoader.getImplementation(this.mirrorName)
        let opts = Object.assign({}, default_test_options) // default options first
        if (impl.test_options) {
            opts = Object.assign(opts, impl.test_options) // override those defined in implementation
        }
        opts = Object.assign(opts, this.options) // override with those added to call the runner
        this.options = opts
        this.search = this.options.search_phrase
    }
    /**
     * A stream to write to
     * @param {response stream} res 
     */
    setResponseStream(res) {
        this.responseStream = res
        this.wroteToArray = false
    }

    writeResultElementInArray(testResult) {
        if (this.responseStream) {
            let writable = this.getReadableUnit(testResult)
            if (writable !== null) {
                this.responseStream.write((this.wroteToArray ? "," : "") + JSON.stringify(writable))
                this.wroteToArray = true
            }
        }
    }
    /**
     * Run test suite
     */
    async run() {
        console.log("Running test suite for " + this.mirrorName)
        await this.loadTestOptions() // load options for the test suite
        await this.loadTests()
    }

    /**
     * Runs the test course
     */
    async loadTests(step = 0, substep = 0) {
        let canFail = []
        if (this.options.do_not_fail_if_chapters_empty) {
            canFail.push({
                name: "list of chapters",
                attempts: this.options.chapters_attempt
            })
        }
        //initialize values
        if (step === substep && step === 0) {
            this.testsResults = [];
            this.currentTest = 0;
            this.forcedValues = {};
            this.outputs = {};
        }
        let forceVal = "__FORCED__"
        if (this.options.manga_url !== undefined) {
            step = 2
            this.outputs["searchList"] = [{text:forceVal, value:this.options.manga_url}]
            this.testsResults[0] = {};
            this.testsResults[0].output = [];
            this.testsResults[1] = {};
            this.testsResults[1].output = [{
                name: "searchList", 
                value: this.outputs["searchList"],
                currentValue: this.options.manga_url}]
            this.forcedValues["searchList"] = this.options.manga_url
        }
        if (this.options.chapter_url !== undefined) {
            step = 4
            this.outputs["searchList"] = [{text:forceVal, value:forceVal}]
            this.testsResults[0] = {};
            this.testsResults[0].output = [];
            this.testsResults[1] = {};
            this.testsResults[1].output = [{
                name: "searchList", 
                value: this.outputs["searchList"],
                currentValue: forceVal}]
            this.forcedValues["searchList"] = forceVal
            this.outputs["chaptersList"] = [{text:forceVal, value:this.options.chapter_url}]
            this.testsResults[2] = {};
            this.testsResults[2].output = [{
                name: "chaptersList", 
                value: this.outputs["chaptersList"],
                currentValue: this.options.chapter_url}]
            this.testsResults[3] = {};
            this.testsResults[3].output = [];
        }
        let mirror = await ImplLoader.getDescription(this.mirrorName)
        let i = 0;
        while (i < tests.length) {
            let test = tests[i]
            i++;
            if (step !== 0 && i <= step) continue;
            this.currentTest = i - 1;
            let breakingFail = false;
            try {
                let passed = true; // result of current test
                let results = []; // text results of sub tests of the test
                let testouts = [];
                if (test.set) { // values to set before testing
                    for (let toset of test.set) {
                        let spl = toset.split(" ");
                        if (spl.length > 1) {
                            if (!this.outputs[spl[1]])
                                throw({message:"the required input " + spl[1] + " is missing."});
                            if (spl[0] === "oneof") {
                                // select one entry in previous output list
                                this.selectEntryRandomly(spl[1]);
                            }
                        }
                    }
                }
                for (let unit of test.tests) {
                    let isok, text; // return from the test function
                    let tocall; // test function to call
                    let inputs = []; // inputs of the test function (first parameter is mirror)
                    let output; // output of the test function
                    if (typeof unit === "function") {
                        tocall = unit;
                    } else {
                        tocall = unit.test;
                        // bind inputs
                        if (unit.input) {
                            for (let inp of unit.input) {
                                let spl = inp.split(" ");
                                if (spl.length > 1) {
                                    let outname = spl[1];
                                    let optional = false;
                                    if (outname.indexOf("optional:") === 0) {
                                        optional = true;
                                        outname = outname.substr(9)
                                    }
                                    if (!optional && !this.outputs[outname])
                                        throw({message:"the required input " + outname + " is missing."});
                                    if (this.outputs[outname]) {
                                        if (spl[0] === "oneof") {
                                            // select one entry in previous output
                                            inputs.push(this.selectEntryRandomly(outname));
                                        } else if (spl[0] === "valueof") {
                                            inputs.push(this.getEntryValue(outname));
                                        } else if (spl[0] === "textof") {
                                            inputs.push(this.getEntryText(outname));
                                        }
                                    } else {
                                        inputs.push(undefined)
                                    }
                                } else {
                                    let optional = false;
                                    if (inp.indexOf("optional:") === 0) {
                                        optional = true;
                                        inp = inp.substr(9)
                                    }
                                    if (!optional && !this.outputs[inp])
                                        throw({message:"the required input " + inp + " is missing."});
                                    if (this.outputs[inp]) {
                                        inputs.push(this.outputs[inp]);
                                    } else {
                                        inputs.push(undefined)
                                    }
                                }
                            }
                        }
                    }
                    try {
                        let ress = await tocall.bind(this)(mirror, ...inputs);
                        //console.log(ress) // display progerssion of tests
                        let allres = [];
                        // if there is only one result, add it to the result array
                        if (ress.length > 0 && (ress[0] === true || ress[0] === false)) { 
                            allres.push(ress);
                        } else {
                            allres.push(...ress);
                        }
                        let cur_outputs = []
                        for ([isok, text, ...cur_outputs] of allres) { // for all results of unit test
                            if (unit.output !== undefined && unit.output.length > 0 && cur_outputs.length > 0) {
                                for (let o = 0; o < cur_outputs.length; o++) {
                                    let output = cur_outputs[o]
                                    let name = unit.output[o]
                                    this.outputs[name] = output; // save the output
                                    testouts.push({
                                        name: name,
                                        value: output,
                                        display: unit.display !== undefined && unit.display.length >= o+1 ? unit.display[o] : undefined,
                                        currentValue: undefined, 
                                        buttons: unit.buttons !== undefined && unit.buttons.length >= o+1 ? unit.buttons[o] : undefined
                                    });
                                }
                            }
                            passed &= isok; // check if test passed
                            if (!isok) {
                                results.push("Failed : " + text);
                            } else {
                                results.push(text);
                            }
                        }
                    } catch (e) {
                        passed = false;
                        breakingFail = true;
                        results.push("Error while running test : " + (e === null ? "null" : e.name + " : " + e.message + "\n" + e.stack));
                    }
                }
                if (!passed && canFail.length > 0) {
                    let testCanFail = canFail.find(conds => conds.name === test.name)
                    if (testCanFail !== undefined && testCanFail.attempts > 0) {
                        console.log("Failed test " + test.name + " --> Retry")
                        testCanFail.attempts = testCanFail.attempts - 1;
                        i = i - 1;
                        continue;
                    }
                }
                // global object containing tests results
                this.testsResults[this.currentTest] = {
                    passed: passed, // result of the test
                    results: results, // list of text unit results
                    output: testouts // array of outputs
                }
                this.writeResultElementInArray(this.testsResults[this.currentTest])
                if (breakingFail) {
                    break;
                }
            } catch (e) {
                if (canFail.length > 0) {
                    let testCanFail = canFail.find(conds => conds.name === test.name)
                    if (testCanFail !== undefined && testCanFail.attempts > 0) {
                        console.log("Failed test " + test.name + " --> Retry")
                        testCanFail.attempts = testCanFail.attempts - 1;
                        i = i - 1;
                        continue;
                    }
                }
                console.error(e)
                this.testsResults[this.currentTest] = {
                    passed: false, // result of the test
                    results: ["Failed : " + (e === null ? "null" : e.name + " : " + e.message + "\n" + e.stack)], // list of text unit results
                    output: []
                }
                this.writeResultElementInArray(this.testsResults[this.currentTest])
                break;
            }
        }
    }

    /**
     * Select a value randomly in an output
     */
    selectEntryRandomly(outputName) {
      if (this.forcedValues[outputName]) return this.forcedValues[outputName]
      for (let i = 0; i < this.currentTest; i++) {
        for (let out of this.testsResults[i].output) {
          if (out.name === outputName) {
            //select a random value
            let val =
              out.value[Math.floor(Math.random() * out.value.length)].value;
            //set it as model
            out.currentValue = val;
            //return it
            return val;
          }
        }
      }
      return undefined;
    }

    /**
     * Return the value of an output
     */
    getEntryValue(outputName) {
      for (let i = 0; i < this.currentTest; i++) {
        for (let out of this.testsResults[i].output) {
          if (out.name === outputName) {
            //return it
            return out.currentValue;
          }
        }
      }
      return undefined;
    }

    /**
     * Return the text of an output
     */
    getEntryText(outputName) {
      for (let i = 0; i < this.currentTest; i++) {
        for (let out of this.testsResults[i].output) {
          if (out.name === outputName) {
            //return the text
            return out.value.find(el => el.value === out.currentValue).text;
          }
        }
      }
      return undefined;
    }

    getResultObject() {
        return this.testsResults
    }

    getReadableUnit(testResult) {
        if (!testResult.results) return null
        let read = {
            html: testResult.results.map(str => "<p>" + linkifyHtml(str, {target: '_blank'}) + "</p>").join("")
        }
        if (testResult.passed !== 1)
            read.failed = true
        return read
    }
    getReadableResult() {
        return this.testsResults.filter(res => res.results).map(res => this.getReadableUnit(res))
    }

    getTestInfos() {
        return {
            manga_url: this.getEntryValue("searchList"),
            manga_name: this.getEntryText("searchList"),
            chapter_url: this.getEntryValue("chaptersList")
        }
    }
}

