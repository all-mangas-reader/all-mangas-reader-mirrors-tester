/**
 * Entry point of the tester
 */
const WebsiteTestRunner = require('./ws-testrunner')
const ImplLoader = require("./impl/impl-loader")
const moment = require("moment")

module.exports = function (mirror) {
    this.mirror = mirror

    this.runAll = async function () {
        let wss = await ImplLoader.getWebsites()
        let failed = []
        let nbtested = 0;
        for (let name in wss) {
            if (!wss.hasOwnProperty(name)) continue;
            let ws = wss[name]
            if (ws.type === "abstract" || name === "__LOADED__") continue;
            nbtested++;
            let result = await this.runMirror(ws.mirrorName)
            if (this.hasFailed(result)) failed.push(ws.mirrorName)
        }
        if (failed.length === 0) {
            console.log("All " + nbtested + " websites passed successfully the test suite")
        } else {
            console.log(failed.length + " / " + nbtested + " websites failed the test suite : " + failed.join(", "))
        }
    }
    this.hasFailed = function (readable) {
        for (let test of readable) {
            if (test.failed) return true
        }
        return false
    }
    /**
     * Run test suite for a mirror
     * @param {A response stream to send data to} res 
     * @param {Additionnal options of test suite} options 
     */
    this.runMirror = async function (res, options = {}) {
        let name = this.mirror
        let wss = await ImplLoader.getWebsites()
        let sname = this.safename(name)
        for (let n in wss) {
            if (this.safename(n) === sname) {
                name = n
                break
            }
        }
        let runner = new WebsiteTestRunner(name, options)
        /** Write beggining of response stream */
        if (res) res.write("{\"name\":\"" + name + "\",\"time\":\"" + moment().format() + "\",\"results\":[")
        /** Set response stream on runner */
        runner.setResponseStream(res)
        /** Run the test suite */
        await runner.run()
        let readable = runner.getReadableResult()
        let infos = runner.getTestInfos()
        /** Write end of response stream */
        if (res) res.write("],\"infos\":" + JSON.stringify(infos) + ",\"success\":" + !this.hasFailed(readable) + "}")
        console.log(this.hasFailed(readable) ? name + " Failed" : name + " Successed")
        return {
            results: readable,
            infos: infos,
            success: !this.hasFailed(readable)
        }
    }
    this.safename = function (name) {
        if (name == undefined || name == null || name == "null")
            return "";
        return name.trim().replace(/[^0-9A-Za-z]/g, '').toLowerCase();
    }
}
