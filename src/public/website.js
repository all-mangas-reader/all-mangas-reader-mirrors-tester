(function() {
    function renderPartResponse(resp, time) {
        document.getElementById("result").innerHTML = "";
        let res = document.createElement("h2")
        if (resp.success) {
            res.innerHTML = "Success !"
            res.className = "success"
        } else if (resp.success === false) {
            res.innerHTML = "Failed"
            res.className = "error"
        } else {
            res.innerHTML = "Running test suite..."
        }
        document.getElementById("result").appendChild(res)
        let sptime = document.createElement("div")
        sptime.innerHTML = "Test suite executed in " + time + " ms"
        sptime.className = "exec-time"
        document.getElementById("result").appendChild(sptime)
        for (let r of resp.results) {
            let respart = document.createElement("div")
            respart.innerHTML = r.html
            if (r.failed) {
                respart.className = "result failed"
            } else {
                respart.className = "result"
            }
            document.getElementById("result").appendChild(respart)
        }
    }
    let ts = Date.now()
    var xhr = new XMLHttpRequest();
    var mirrorName = document.getElementById("name").value
    xhr.open('POST', "/runtest/" + mirrorName, true);
    xhr.seenBytes = 0;

    let globalRes = "";
    xhr.onreadystatechange = function() {
        let time = (Date.now() - ts) / 1000
        if(xhr.readyState === 3) {
          var newData = xhr.response.substr(xhr.seenBytes);
          globalRes += newData;
          let parsed = null;
          try {
            parsed = JSON.parse(globalRes)
          } catch (e) {
            try {
                parsed = JSON.parse(globalRes + "]}")
            } catch (e) {}
          }
          if (parsed !== null) renderPartResponse(parsed, time)
          xhr.seenBytes = xhr.responseText.length;
        }
    };
      
    xhr.onload = function(e) {
        let time = (Date.now() - ts) / 1000
        let resp = JSON.parse(this.response);
        renderPartResponse(resp, time)
    }

    let params = [];
    let inputsearch = document.getElementById("actions").elements["search_phrase"]
    let inputmg = document.getElementById("actions").elements["manga_url"]
    let inputchap = document.getElementById("actions").elements["chapter_url"]
    if (inputsearch.value !== "") params.push("search_phrase=" + inputsearch.value)
    if (inputmg.value !== "") params.push("manga_url=" + inputmg.value)
    if (inputchap.value !== "") params.push("chapter_url=" + inputchap.value)
    
    if (params.length > 0) {
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    }
    xhr.send(params.join("&"));
})()