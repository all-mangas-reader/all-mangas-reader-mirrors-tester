/**
 * Hook for the util function needed from tests
 * tests are the same as those from the laboratory in amr extension, just hook the 
 * util functions needed there
 */
module.exports = {
    /**
     * Extract the full host name
     * @param {*} url 
     */
    extractHostname: function(url) {
        var hostname;
        //find & remove protocol (http, ftp, etc.) and get hostname

        if (url.indexOf("://") > -1 || url.indexOf("//") === 0) {
            hostname = url.split('/')[2];
        }
        else {
            hostname = url.split('/')[0];
        }

        //find & remove port number
        hostname = hostname.split(':')[0];
        //find & remove "?"
        hostname = hostname.split('?')[0];

        return hostname;
    },

    /**
     * List of supported languages in AMR, some are not traditional language codes
     * Multiple codes can match a same language, in this case, the entry is a list of 
     * codes matching a language
     * Each code must have a css rule in flags.css to display the right flag for the language code
     * and a message code to display the language name
     */
    languages: [
        ["ar", "sa"], "bd", "bg", "ct", "cn", "hk", "cz", "dk", "nl", ["en", "gb"], "ph", "fi", "fr", "de", "gr", "hu", "id", "it", "jp", "kr", "my", "mn", "ir", "pl", "br", "pt", "ro", "ru", "rs", "es", "mx", "se", "th", "tr", "ua", "vn"
    ],

    /**
     * Test if an url match an url pattern containing wildcards
     * @param {*} str a domain name
     * @param {*} rule 
     */
    matchDomain: function(str, rule) {
        let doMatch = new RegExp("^" + rule.replace(/\*/g, '.*') + "$").test(str)
        return doMatch
    }
}