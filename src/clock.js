const { exec } = require('child_process');

var loadTests = function() {
    exec('node ./src/index.js', (err, stdout, stderr) => {
        if (err) {
            console.error(err)
            return;
        }
    });
}

var { CronJob } = require('cron');
new CronJob({
  cronTime: "0 22 * * * *", // 10pm each day
  onTick: loadTests,
  start: true,
  timeZone: "Europe/Paris"
});