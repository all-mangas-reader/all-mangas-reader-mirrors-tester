var request = require('request')

var ImplLoader = function() {
    this.implementations = []
    this.websites = []
    //this.repo = "http://localhost:8081/" 
    this.repo = "https://mirrors.allmangasreader.com/v4/"

    this.resetWebsites = function() {
        this.websites = []
    }

    this.getWebsites = async function() {
        if (this.websites["__LOADED__"] === undefined) {
            console.log("Loading websites definition")
            await this.loadWebsites()
        }
        return this.websites
    }

    /**
     * Return the implementation for a mirror
     * @param {*} desc 
     * @param {*} isAbstract 
     */
    this.getImplementation = async function(mirrorName) {
        let self = this
        // DO NOT CACHE IMPLEMENTATIONS, RELOAD ON EACH CALL
        /*if (this.implementations[mirrorName] !== undefined) {
            if (this.implementations[mirrorName].mirrorName !== undefined || // impl is loaded
                typeof this.implementations[mirrorName] === 'function') { // impl is abstract (instanciable)
                // object loaded
                return Promise.resolve(this.implementations[mirrorName])
            }
            // wait for object to be loaded
            return new Promise((resolve, reject) => {
                (function waitForInit() {
                    if (self.implementations[mirrorName].mirrorName) 
                        resolve(self.implementations[mirrorName]);
                    else
                        setTimeout(waitForInit, 10);
                })()
            })
        }*/

        this.implementations[mirrorName] = {}
        let desc = await this.getDescription(mirrorName)
        let isAbstract = desc.type === 'abstract'
        return new Promise((resolve, reject) => {
            request(
                self.repo + desc.jsFile + "?ts=" + Date.now(), 
                function(error, response, body) {
                    if (!error && response.statusCode === 200) {
                        (async () => {
                            var registerMangaObject = async function(object) {
                                if (object.abstract !== undefined) {
                                    // The mirror is abstract --> load the abstract impl
                                    await self.getImplementation(object.abstract)
                                    let nobj = new global[object.abstract](object.abstract_options)
                                    object = Object.assign(nobj, object)
                                }
                                self.implementations[mirrorName] = object
                                resolve(self.implementations[mirrorName])
                            }
                            // If the implementation to load is abstract, add new AbstractName() to the script so the eval will return a new instance of the abstract
                            var resultEval = eval(
                                body + (isAbstract ? "; " + desc.mirrorName : "")
                            )
                            if (isAbstract) {
                                self.implementations[mirrorName] = resultEval
                                global[desc.mirrorName] = resultEval
                                resolve(self.implementations[mirrorName])
                            }
                        })()
                    } else {
                        console.error(error)
                        reject(error)
                    }
                }
            )
        })
    }

    /**
     * Get mirror description from websites.json
     * @param {*} mirrorName 
     */
    this.getDescription = async function(mirrorName) {
        let self = this
        return new Promise(async (resolve, reject) => {
            if (self.websites["__LOADED__"] === undefined) {
                await self.loadWebsites()
            }

            if (self.websites[mirrorName] !== undefined)
                resolve(self.websites[mirrorName])
            else
                reject("Unknown website mirror " + mirrorName)
        })
    }
    /**
     * Loads the website descriptions
     */
    this.loadWebsites = async function() {
        let self = this
        return new Promise((resolve, reject) => {
            request({
                url: self.repo + "websites.json", 
                headers: {"Cache-Control": "no-cache", "Pragma": "no-cache"}
            }, function(error, response, body) {
                if (!error && response.statusCode === 200) {
                    let wss = JSON.parse(body)
                    for (let ws of wss) {
                        self.websites[ws.mirrorName] = ws
                    }
                    self.websites["__LOADED__"] = true
                    resolve()
                } else {
                    console.error(error)
                    reject(error)
                }
            })
        })
    }
}

module.exports = new ImplLoader()