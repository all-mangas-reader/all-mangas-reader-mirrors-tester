# All Mangas Reader Mirrors Test Suite

This project is a NodeJS web application running on Heroku. It aims to test if websites implementations from project https://gitlab.com/all-mangas-reader/all-mangas-reader-2-mirrors work well.

You can test the Heroku app here : https://all-mangas-reader-mirrors-test.herokuapp.com

Implementations for a website are subject to fail because of updates on the website itself. **Thanks to the test suite, we are able to be notified within a day that the implementation does not work anymore**

The main page of the app is a list of every implementation published on AMR. Click on a website to run the whole test suite for it.

On the website page, you can force some values for the test suite : 
 - search phrase : change the default search phrase
 - manga url : set the manga url you want to test
 - chapter url : set the chapter url you want to test

### Developers
You can run this test suite locally using `yarn run start` after having run `yarn install`

To help you in your implementation development, change the repo variable in `src/impl/impl-loader.js` to your local mirrors server `http://localhost:8080`
